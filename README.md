<h3 align="center">

> Traders Diary

</h3>

_Em desenvolvimento..._

![Imagem plataforma traders diary](./src/assets/traders_diary.png)

#### Gerencia seus trading

- Selecione qual ativo você está operando
- Horário de entrada e saída em cada operação
- Tipo de operação (Scalper..)
- Numero de contratos (para ativos do mercado futuro)
- Descreva com detalhes a estrátegia usada na operação
- Calculo do total de pontos ganhos ou perdidos na operação

#### Charts

- Gráfico com informações que ajudam a refinar as estrátegias usada em cada operação
- Total de acertos
- Gains | Loss

![Imagem plataforma traders diary total operations](./src/assets/traders_diary_total_operations.png)
