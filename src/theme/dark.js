export default {
  primary: "#121212",
  secondary: "#1f1f1f",
  foreground: "#fff"
};
