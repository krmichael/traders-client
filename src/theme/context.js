import React from "react";
import light from "./light";

export default React.createContext({
  theme: JSON.parse(localStorage.getItem("theme")) || light
});
