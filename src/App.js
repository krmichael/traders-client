import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ThemeProvider } from "styled-components";

import * as themes from "./theme";
import ThemeContext from "./theme/context";

import GlobalStyle from "./styles/global";
import Header from "./components/Header";

import Home from "./pages/Home";
import Charts from "./pages/Charts";
import Trade from "./pages/Trade";

class App extends Component {
  state = {
    theme: JSON.parse(localStorage.getItem("theme")) || themes.light
  };

  toggleTheme = () => {
    this.setState(
      state => ({
        theme: state.theme === themes.dark ? themes.light : themes.dark
      }),
      localStorage.setItem(
        "theme",
        JSON.stringify(
          this.state.theme === themes.dark ? themes.light : themes.dark
        )
      )
    );
  };

  render() {
    return (
      <ThemeContext.Provider value={this.state}>
        <ThemeContext.Consumer>
          {theme => (
            <ThemeProvider theme={theme}>
              <GlobalStyle />

              <Router>
                <Header />

                <Switch>
                  <Route exact={true} path="/">
                    <Home toggleTheme={this.toggleTheme} />
                  </Route>

                  <Route path="/charts">
                    <Charts />
                  </Route>

                  <Route path="/trade">
                    <Trade />
                  </Route>
                </Switch>
              </Router>
            </ThemeProvider>
          )}
        </ThemeContext.Consumer>
      </ThemeContext.Provider>
    );
  }
}

export default App;
