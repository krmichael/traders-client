export default function formatDate(date) {
  let format = {
    year: date.getFullYear(),
    month:
      date.getMonth() + 1 < 10
        ? "0" + (date.getMonth() + 1)
        : date.getMonth() + 1,
    day: date.getDate() < 10 ? "0" + date.getDate() : date.getDate()
  };

  return `${format.year}-${format.month}-${format.day}`;
}
