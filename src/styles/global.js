import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  * {
    padding: 0;
    margin: 0;
    border: none;
    outline: none;
    box-sizing: border-box;

    ::-webkit-scrollbar {
      width: 0px;
      height: 0px;
    }
  }

  html, body, #root {
    width: 100%;
    height: 100%;
    color: ${props => props.theme.theme.foreground};
    background-color: ${props => props.theme.theme.secondary};
    font-family: Arial, Helvetica, sans-serif;
  }
`;

export default GlobalStyle;
