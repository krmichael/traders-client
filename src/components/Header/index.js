import React from "react";
import { useHistory } from "react-router-dom";

import { mdiDialpad, mdiFinance, mdiCloudUploadOutline } from "@mdi/js";

import { Container, Scope, Logo, LogoText, Icon, Icons } from "./styles";

export default function Header() {
  const history = useHistory();

  return (
    <Container>
      <Scope>
        <Logo onClick={() => history.push("/")}>
          <Icon path={mdiDialpad} size={1} style={{ cursor: "pointer" }} />

          <LogoText>Traders</LogoText>
        </Logo>

        <Icons>
          <Icon
            path={mdiFinance}
            size={1}
            style={{ cursor: "pointer", marginRight: 15 }}
            onClick={() => history.push("/charts")}
          />

          <Icon
            path={mdiCloudUploadOutline}
            size={1}
            style={{ cursor: "pointer" }}
            onClick={() => history.push("/trade")}
          />
        </Icons>
      </Scope>
    </Container>
  );
}
