import styled from "styled-components";
import MDIIcon from "@mdi/react";

export const Container = styled.header`
  position: fixed;
  width: 100%;
  padding: 15px 25px;
  background-color: ${props => props.theme.theme.secondary};
  box-shadow: 0px 3px 7px ${props => props.theme.theme.primary};
`;

export const Icon = styled(MDIIcon).attrs(props => ({
  color: `${props.theme.theme.foreground}`
}))``;

export const Scope = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  max-width: 1280px;
  margin: 0px auto;
`;

export const Logo = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`;

export const LogoText = styled.h2`
  margin-left: 3px;
  margin-top: -4px;
  font-size: 24px;
  font-weight: bold;
`;

export const Icons = styled.div`
  display: flex;
  align-items: center;
`;
