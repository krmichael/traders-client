import React from "react";

import { Select } from "./styles";

export default function SelectComponent({
  data = [],
  value,
  onChange,
  style,
  ...props
}) {
  return (
    <Select value={value} onChange={onChange} style={style} {...props}>
      {data.length > 0 &&
        data.map(option => (
          <option key={option.id} value={option.value}>
            {option.value}
          </option>
        ))}
    </Select>
  );
}
