import styled from "styled-components";

export const Select = styled.select`
  width: 300px;
  padding: 7px;
  cursor: pointer;
  border-radius: 3px;
  border: 0.5px solid ${props => props.theme.theme.secondary};
  background-color: ${props => props.theme.theme.secondary};
  color: ${props => props.theme.theme.foreground};
  box-shadow: 0px 3px 7px ${props => props.theme.theme.primary};
`;
