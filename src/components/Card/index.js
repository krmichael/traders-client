import React from "react";

import { Container, Active, ActiveText, Value, ValueText } from "./styles";

export default function Card({ active, value, negative, onClick }) {
  return (
    <Container onClick={onClick}>
      <Active>
        <ActiveText>{active}</ActiveText>
      </Active>

      <Value>
        <ValueText negative={negative}>{value}</ValueText>
      </Value>
    </Container>
  );
}
