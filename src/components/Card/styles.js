import styled from "styled-components";

export const Container = styled.div`
  height: 100px;
  display: flex;
  flex: 1 1 250px;
  padding: 7px;
  border-radius: 3px;
  margin-bottom: 10px;
  border: 0.5px solid ${props => props.theme.theme.primary};
  flex-direction: column;
  justify-content: space-between;
  box-shadow: 0px 3px 7px ${props => props.theme.theme.primary};
  cursor: pointer;
  &:not(:last-child) {
    margin-right: 7px;
  }
`;

export const Active = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const ActiveText = styled.h3`
  font-size: 18px;
  margin-top: 10px;
  letter-spacing: 1px;
`;

export const Value = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const ValueText = styled.h3`
  font-size: 14px;
  font-weight: 600;
  color: ${props => (props.negative ? "#f44336" : "#34d13a")};
`;
