import styled from "styled-components";

export const Container = styled.div`
  position: fixed;
  background-color: ${props => props.theme.theme.secondary};
  display: flex;
  justify-content: flex-end;
  left: 0px;
  right: 0px;
  bottom: 0px;
  padding: 15px 25px;
  box-shadow: 0px -3px 7px ${props => props.theme.theme.primary};
`;

export const Accumulated = styled.p`
  font-size: 14px;
`;

export const AccumulatedValue = styled.span`
  font-size: 14px;
  font-weight: bold;
  color: ${props => (props.negative ? "#f44336" : "#34d13a")};
`;
