import React from "react";

import { Container, Accumulated, AccumulatedValue } from "./styles";

export default function Footer({ accumulated = 0 }) {
  return (
    <Container>
      <Accumulated>
        Accumulated value:{" "}
        <AccumulatedValue negative={parseInt(accumulated, 10) < 0}>
          {parseInt(accumulated, 10) > 0 ? "+" + accumulated : accumulated}
        </AccumulatedValue>
      </Accumulated>
    </Container>
  );
}
