import React from "react";
import Calendar from "@wojtekmaj/react-daterange-picker";

export default function CalendarComponent({
  visible,
  value,
  onChange,
  ...props
}) {
  if (visible) {
    return <Calendar value={value} onChange={onChange} {...props} />;
  }

  return null;
}
