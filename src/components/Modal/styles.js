import styled, { keyframes } from "styled-components";

const FadeIn = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

const TopDown = keyframes`
  from {
    margin-top: -2000px;
  }

  to {
    margin-top: 0px;
  }
`;

export const Container = styled.div`
  position: fixed;
  overflow: hidden;
  top: 0px;
  left: 0px;
  bottom: 0px;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.7);
  animation: ${FadeIn} 0.2s ease-in-out;
`;

export const Scope = styled.div`
  max-width: 780px;
  width: 780px;
  height: 500px;
  background-color: ${props => props.theme.theme.secondary};
  box-shadow: 0px 3px 3px ${props => props.theme.theme.primary};
  animation: ${TopDown} 0.5s ease-in-out;
`;

export const ScopeTitle = styled.div`
  display: flex;
  margin: 10px;
`;

export const Title = styled.h3`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const StrategyItems = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 25px;
  background-color: ${props => props.theme.theme.primary};
  box-shadow: 0px 3px 3px ${props => props.theme.theme.primary};
`;

export const Items = styled.h3`
  font-size: 14px;
  font-weight: 600;
  letter-spacing: 0.7;
`;

export const Description = styled.div`
  margin: 20px;
  overflow: scroll;
  height: 400px;
  padding: 10px 0px;
`;

export const DescriptionText = styled.p`
  font-size: 16px;
  letter-spacing: 1px;
`;
