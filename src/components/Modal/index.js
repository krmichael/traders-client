import React from "react";

import Icon from "@mdi/react";
import { mdiCloseCircle } from "@mdi/js";

import {
  Container,
  Scope,
  ScopeTitle,
  Title,
  StrategyItems,
  Items,
  Description,
  DescriptionText
} from "./styles";

export default function Modal({
  visible,
  title,
  close,
  content = false,
  children,
  active,
  operation,
  contracts,
  time,
  points,
  description
}) {
  if (visible) {
    return (
      <Container>
        <Scope>
          <ScopeTitle>
            <Title>{title}</Title>

            <Icon
              onClick={close}
              path={mdiCloseCircle}
              size={1.5}
              color="#34d13a"
              style={{ cursor: "pointer" }}
            />
          </ScopeTitle>

          {content ? (
            children
          ) : (
            <>
              <StrategyItems>
                <Items>Active: {active}</Items>
                <Items>Operation: {operation}</Items>
                <Items>Number of Contracts: {contracts}</Items>
                <Items>Time: {time}</Items>
                <Items>Points: {points}</Items>
              </StrategyItems>

              <Description>
                <DescriptionText>{description}</DescriptionText>
              </Description>
            </>
          )}
        </Scope>
      </Container>
    );
  } else {
    return null;
  }
}
