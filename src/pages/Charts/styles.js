import styled from "styled-components";

export const Container = styled.div`
  height: 100%;
  padding-top: 70px;
`;

export const ChartContainer = styled.div`
  width: 100%;
  height: 100%;
  margin: 20px 0px auto auto;
`;

export const Canvas = styled.canvas`
  width: calc(100% - 100px) !important;
  height: calc(100% - 100px) !important;
  margin: 0px auto;
`;
