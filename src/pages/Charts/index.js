import React, { useState, useEffect } from "react";

import FinanceIcon from "@mdi/react";
import { mdiFinance } from "@mdi/js";

import { Nothing, NothingText } from "../Home/styles";
import { Container } from "./styles";
import Chart from "./chart";
import { url } from "../../services";

export default function Charts() {
  const [traders, setTraders] = useState([]);

  async function getData() {
    await fetch(url)
      .then(res => {
        if (res.ok) return res.json();
        return res.statusText;
      })
      .then(response => {
        setTraders(response.data);
      })
      .catch(err => console.error(err));
  }

  useEffect(() => {
    getData();
  }, []);

  return (
    <Container>
      {traders.length > 0 ? (
        <Chart data={traders} />
      ) : (
        <Nothing>
          <FinanceIcon path={mdiFinance} size={7} color="#4caf50" />
          <NothingText>No chart for show this time!</NothingText>
        </Nothing>
      )}
    </Container>
  );
}
