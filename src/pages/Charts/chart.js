import React, { useRef, useEffect } from "react";
import Chart from "chart.js";

import { ChartContainer, Canvas } from "./styles";

export default function ChartComponent({ data = [] }) {
  const chartRef = useRef();

  const tradersCount = data.length;

  useEffect(() => {
    function renderChart() {
      const ctx = chartRef.current;

      const lineChart = new Chart(ctx, {
        type: "line",
        data: {
          labels:
            data.length > 0 &&
            data.map(t => (parseInt(t.Points, 10) > 0 ? "Gain" : "Loss")),
          datasets: [
            {
              label: "Points",
              data: data.length > 0 && data.map(t => t.Points),
              borderColor: ["rgba(0, 150, 136, 1)"],
              borderWidth: 1,
              backgroundColor: "transparent",
              pointBackgroundColor:
                data.length > 0 &&
                data.map(t =>
                  parseInt(t.Points, 10) > 0
                    ? "rgba(76, 175, 80, 1)"
                    : "rgba(244, 67, 54, 1)"
                ),
              pointBorderColor:
                data.length > 0 &&
                data.map(t =>
                  parseInt(t.Points, 10) > 0
                    ? "rgba(76, 175, 80, 1)"
                    : "rgba(244, 67, 54, 1)"
                ),
              pointStyle: "rectRot",
              pointRadius: 5
            }
          ]
        },
        options: {
          legend: {
            display: false
          },
          title: {
            display: true,
            text: `Total operations: ${tradersCount || 0}`,
            fontSize: 20,
            fontStyle: "bold",
            padding: 30
          }
        }
      });

      return lineChart;
    }

    renderChart();
  }, [data, tradersCount]);

  return (
    <ChartContainer>
      <Canvas ref={chartRef} />
    </ChartContainer>
  );
}
