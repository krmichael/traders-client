import styled from "styled-components";
import MDIIcon from "@mdi/react";

export const Container = styled.div`
  height: 100%;
  padding: 15px 25px;
`;

export const Scope = styled.div`
  display: flex;
  justify-content: flex-end;
  max-width: 1280px;
  margin: 50px auto 0px auto;
`;

export const ScopeCalendar = styled.div`
  text-align: center;

  div.react-daterange-picker__wrapper {
    padding: 7px;
    border-radius: 3px;
    border: 0.5px solid ${props => props.theme.theme.primary};
    background-color: ${props => props.theme.theme.secondary};
    color: ${props => props.theme.theme.foreground};
    box-shadow: 0px 3px 7px ${props => props.theme.theme.primary};
    margin-bottom: 5px;

    .react-daterange-picker__inputGroup
      > input.react-daterange-picker__inputGroup__input {
      color: ${props => props.theme.theme.foreground};
    }

    span.react-daterange-picker__range-divider {
      display: flex;
      justify-content: center;
      align-items: center;
    }

    svg.react-daterange-picker__calendar-button__icon,
    svg.react-daterange-picker__button__icon {
      stroke: ${props => props.theme.theme.foreground};
    }
  }

  div.react-daterange-picker__calendar.react-daterange-picker__calendar--open {
    div.react-calendar {
      border-radius: 3px;
      border: 0.5px solid ${props => props.theme.theme.primary};
      background-color: ${props => props.theme.theme.secondary};
      color: ${props => props.theme.theme.foreground};
      box-shadow: 0px 3px 7px ${props => props.theme.theme.primary};

      div.react-calendar__navigation > button {
        background-color: ${props => props.theme.theme.secondary};
        color: ${props => props.theme.theme.foreground};
        box-shadow: 0px 3px 7px ${props => props.theme.theme.primary};
      }
    }
  }
`;

export const ScopeIcon = styled.span`
  width: 30px;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

export const Icon = styled(MDIIcon).attrs(props => ({
  color: `${props.theme.theme.foreground}`
}))`
  float: right;
  cursor: pointer;
`;

export const Operations = styled.div`
  display: flex;
  max-width: 1100px;
  flex-direction: row;
  flex-wrap: wrap;
  margin: 10px auto 30px auto;
  padding-bottom: 20px;
`;

export const Nothing = styled.div`
  height: calc(100% - 130px);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const NothingText = styled.h3`
  font-size: 18px;
  font-weight: bold;
  letter-spacing: 1px;
  color: #4caf50;
`;
