import React, { useState, useEffect, useMemo } from "react";

import FinanceIcon from "@mdi/react";

import {
  mdiFinance,
  mdiLightbulbOutline,
  mdiCalendar,
  mdiAutorenew
} from "@mdi/js";

import Calendar from "../../components/Calendar";
import { url, formatDate } from "../../services";

import {
  Container,
  Scope,
  ScopeCalendar,
  ScopeIcon,
  Icon,
  Operations,
  Nothing,
  NothingText
} from "./styles";

import Card from "../../components/Card";
import Footer from "../../components/Footer";
import Modal from "../../components/Modal";

export default function Home({ toggleTheme }) {
  const [traders, setTraders] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [calendarVisible, setCalendarVisible] = useState(false);
  const [content, setContent] = useState(false);
  const [modalContent, setModalContent] = useState({});
  const [dateRange, setDateRange] = useState([new Date(), new Date()]);
  const [reset, setReset] = useState(false);

  function handleModal(data) {
    setModalVisible(!modalVisible);

    if (data === true) {
      setContent(data);
      setCalendarVisible(true);
    }

    setModalContent(data);
  }

  function handleFilter(date) {
    setDateRange(date);
    setReset(true);

    let [startDate, endDate] = date;

    getData({ startDate, endDate });
  }

  function handleReset() {
    getData({});
    setReset(false);
  }

  async function getData({ startDate, endDate }) {
    let start = !startDate ? formatDate(new Date()) : formatDate(startDate);
    let end = !endDate ? start : formatDate(endDate);

    try {
      await fetch(`${url}/filter`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ startDate: start, endDate: end })
      })
        .then(res => (res.ok ? res.json() : res.statusText))
        .then(response => {
          setTraders(response.data);
        })
        .catch(err => console.log(err));
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getData({});
  }, []);

  const points = useMemo(
    () => traders?.map(trade => parseInt(trade.Points, 10)),
    [traders]
  );

  const accumulatedCount = useMemo(
    () => points?.reduce((total, amount) => total + amount, 0),
    [points]
  );

  return (
    <Container>
      <Scope>
        {reset && (
          <ScopeIcon onClick={handleReset}>
            <Icon
              title="Reset filter"
              rotate={90}
              spin={true}
              path={mdiAutorenew}
              size={0.7}
              style={{ marginTop: 1 }}
            />
          </ScopeIcon>
        )}

        <ScopeIcon onClick={() => handleModal(true)}>
          <Icon
            title="Filter by date"
            path={mdiCalendar}
            size={0.7}
            style={{ marginTop: 1 }}
          />
        </ScopeIcon>

        <ScopeIcon onClick={toggleTheme}>
          <Icon title="Toggle theme" path={mdiLightbulbOutline} size={0.7} />
        </ScopeIcon>
      </Scope>

      {traders.length > 0 ? (
        <Operations>
          {traders.map(operation => (
            <Card
              key={operation.TradersID}
              negative={parseInt(operation.Points, 10) < 0}
              active={operation.Active}
              value={
                parseInt(operation.Points, 10) > 0
                  ? "+" + operation.Points
                  : operation.Points
              }
              onClick={() => handleModal(operation)}
            />
          ))}
        </Operations>
      ) : (
        <Nothing>
          <FinanceIcon path={mdiFinance} size={7} color="#4caf50" />
          <NothingText>No trade found at this time!</NothingText>
        </Nothing>
      )}

      <Footer accumulated={accumulatedCount} />

      <Modal
        visible={modalVisible}
        title={content ? "Filtrar" : "Strategy"}
        close={() => setModalVisible(!modalVisible)}
        content={content}
        active={modalContent.Active}
        operation={modalContent.Operation}
        contracts={modalContent.Contracts}
        time={`${modalContent.Input} - ${modalContent.Output}`}
        points={modalContent.Points}
        description={modalContent.Strategy}
      >
        {content && (
          <ScopeCalendar>
            <Calendar
              visible={calendarVisible}
              value={dateRange}
              autoFocus={calendarVisible}
              onCalendarClose={() => {
                setModalVisible(!modalVisible);
                setContent(false);
              }}
              onChange={date => handleFilter(date)}
            />
          </ScopeCalendar>
        )}
      </Modal>
    </Container>
  );
}
