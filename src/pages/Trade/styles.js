import styled from "styled-components";

export const Form = styled.form`
  max-width: 780px;
  margin: 0px auto;
  padding: 70px 5px 25px 25px;
`;

export const Scope = styled.div`
  margin-bottom: 5px;
  margin-top: 15px;
  font-size: 18px;
`;

export const Label = styled.label`
  margin-top: 10px;
`;

export const Input = styled.input`
  width: 100%;
  padding: 15px;
  border-radius: 3px;
  border: 0.5px solid ${props => props.theme.theme.primary};
  background-color: ${props => props.theme.theme.secondary};
  color: ${props => props.theme.theme.foreground};
  box-shadow: 0px 3px 7px ${props => props.theme.theme.primary};
  margin-bottom: 10px;
`;

export const Strategy = styled.textarea`
  width: 100%;
  max-width: 100%;
  margin-bottom: 10px;
  height: 120px;
  max-height: 130px;
  padding: 15px;
  border-radius: 3px;
  border: 0.5px solid ${props => props.theme.theme.primary};
  background-color: ${props => props.theme.theme.secondary};
  color: ${props => props.theme.theme.foreground};
  box-shadow: 0px 3px 7px ${props => props.theme.theme.primary};
`;

export const ScopeButton = styled(Scope)`
  display: flex;
  justify-content: flex-end;
`;

export const Button = styled.button`
  width: 250px;
  padding: 10px;
  cursor: pointer;
  font-size: 15px;
  font-weight: bold;
  margin-top: 7px;
  color: ${props => props.theme.theme.foreground};
  background: ${props => props.theme.theme.secondary};
  border: 1px solid ${props => props.theme.theme.primary};
  border-radius: 3px;
  box-shadow: 0px 5px 7px ${props => props.theme.theme.primary};

  &:hover {
    background: ${props => props.theme.theme.primary};
    border: 1px solid ${props => props.theme.theme.primary};
  }
`;
