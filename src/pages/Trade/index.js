import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { url, formatDate } from "../../services";

import {
  Form,
  Scope,
  Label,
  Input,
  Strategy,
  ScopeButton,
  Button
} from "./styles";

import Select from "../../components/Select";

const ACTIVES = [
  { id: 1, value: "Mini Índice" },
  { id: 2, value: "Mini Dólar" },
  { id: 3, value: "Fórex" },
  { id: 4, value: "Nasdaq" },
  { id: 5, value: "S&P500" }
];

const OPERATIONS = [
  { id: 1, value: "Normal" },
  { id: 2, value: "Scalp" }
];

export default function Trade() {
  const [active, setActive] = useState(ACTIVES[0].value);
  const [operation, setOperation] = useState(OPERATIONS[0].value);
  const [input, setInput] = useState("");
  const [contracts, setContracts] = useState(1);
  const [strategy, setStrategy] = useState("");
  const [output, setOutput] = useState("");
  const [points, setPoints] = useState("");

  const history = useHistory();

  async function handleSubmit(event) {
    event.preventDefault();

    if (!input || !contracts || !strategy || !output || !points) return;

    const data = {
      active,
      operation,
      contracts,
      input,
      strategy,
      output,
      points,
      created: formatDate(new Date())
    };

    try {
      await fetch(`${url}/trade`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      })
        .then(res => {
          if (!res.ok) {
            return res.statusText;
          }
        })
        .then(() => history.push("/"))
        .catch(err => console.log(err));
    } catch (error) {
      console.log(error);
    }
  }

  function handleActive(event) {
    setActive(event.target.value);
  }

  function handleOperation(event) {
    setOperation(event.target.value);
  }

  return (
    <Form onSubmit={handleSubmit}>
      <Scope>
        <Label>Active</Label>
      </Scope>
      <Select
        data={ACTIVES}
        value={active}
        onChange={handleActive}
        style={{ width: "100%", marginBottom: 10, padding: 15 }}
      />

      <Scope>
        <Label>Input</Label>
      </Scope>
      <Input
        type="text"
        placeholder="09:00"
        value={input}
        onChange={event => setInput(event.target.value)}
      />

      <Scope>
        <Label>Operation</Label>
      </Scope>
      <Select
        data={OPERATIONS}
        value={operation}
        onChange={handleOperation}
        style={{ width: "100%", marginBottom: 10, padding: 15 }}
      />

      <Scope>
        <Label>Number of Contracts</Label>
      </Scope>
      <Input
        type="text"
        value={contracts}
        onChange={event => setContracts(event.target.value)}
      />

      <Scope>
        <Label>Strategy</Label>
      </Scope>
      <Strategy
        value={strategy}
        onChange={event => setStrategy(event.target.value)}
      />

      <Scope>
        <Label>Output</Label>
      </Scope>
      <Input
        type="text"
        placeholder="09:15"
        value={output}
        onChange={event => setOutput(event.target.value)}
      />

      <Scope>
        <Label>Points</Label>
      </Scope>
      <Input
        type="text"
        placeholder="200"
        value={points}
        onChange={event => setPoints(event.target.value)}
      />

      <ScopeButton>
        <Button type="submit">Save Trade</Button>
      </ScopeButton>
    </Form>
  );
}
